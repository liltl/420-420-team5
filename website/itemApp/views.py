from django.shortcuts import render
from database.models import Profile, Item, Comment
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.urls import reverse


# Create your views here.

#deletes the item
def deleteItem(request):
    #Getting id value from the form
    itemId = request.GET.get('deleteId')
    Item.objects.filter(id=itemId).delete()
    #reloading automatically profile page
    return HttpResponseRedirect(reverse('signUpForm/profile', kwargs={'pk': request.user.id}))


#sends user to the modify page where they can modify their item
def modifyItem(request):
    #Getting id value from the form
    itemId = request.GET.get('modifyId')
    #loading the modify page
    content = {'item': itemId}
    return render(request, 'signUpForm/modify.html', content)


def modifyConfirm(request):
    itemId = request.GET.get('modifyId')
    item = Item.objects.get(id=itemId)
    modified_owner = Profile.objects.get(username=request.user.username)
    
    #if input are empty, they will keep their previous value
    if request.GET.get('name'):
        modified_name = request.GET.get('name')
    else:
        modified_name = item.name
    if request.GET.get('images'):
        modified_images = request.GET.get('images')
    else:
        modified_images = item.images
    if request.GET.get('price'):
        modified_price = request.GET.get('price')
    else:
        modified_price = item.price
    if request.GET.get('description'):
        modified_description = request.GET.get('description')
    else:
        modified_description = item.description
    Item(id=itemId, name=modified_name, owner=modified_owner, images=modified_images, price=modified_price, description=modified_description).save()
    #return to profile page
    return HttpResponseRedirect(reverse('signUpForm/profile', kwargs={'pk': request.user.id}))


#adding item
def addItem(request):
    #Getting value necessary to make an item 
    newItem = Item()
    newItem.owner = Profile.objects.get(username=request.user.username)
    newItem.name = request.GET.get('name')
    newItem.images = request.GET.get('images')
    newItem.price = request.GET.get('price')
    newItem.description = request.GET.get('description')
    
    #adding item to database
    newItem.save()
    
    #reloading the page
    items = Item.objects.all()
    content = {'items': items}
    return HttpResponseRedirect(reverse('signUpForm/profile', kwargs={'pk': request.user.id}))


def search(request):
    query = request.GET.get('search')
    if query:
        items = Item.objects.filter(name__icontains=query)
    else:
        items = Item.objects.all()
    
    content = {'items': items}
    
    if request.user.is_authenticated:
        return render(request, 'signUpForm/userSearch.html', content)
    else:
        return render(request, 'signUpForm/visitorSearch.html', content)
