from django.urls import path
from . import views

urlpatterns = [
        path('delete/', views.deleteItem, name="delete"),
        path('modify/', views.modifyItem, name="modify"),
        path('modifyConfirm/', views.modifyConfirm, name="modifyConfirm"),
        path('add/', views.addItem, name="add"),
    ]