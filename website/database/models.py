from django.db import models

# Create your models here.

# User Model
class Profile(models.Model):

    first_name = models.CharField(max_length=20)
    
    last_name = models.CharField(max_length=20)
    
    authlogged_id = models.IntegerField()
    
    username = models.CharField(max_length=30)
    
    password = models.CharField(max_length=50)
    
    email = models.CharField(max_length=30)
    
    status = models.CharField(max_length=10)

    avatar = models.ImageField(null=True, blank=True, upload_to="images/avatar/")
    
    def __str__(self):
        return self.username
    
# Item Model
class Item(models.Model):
    
    name = models.CharField(max_length=20)
    
    owner = models.ForeignKey(Profile, on_delete=models.CASCADE)
    
    images = models.ImageField(upload_to='media/')
    
    price= models.IntegerField()
    
    description = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name 
    
    
    
# Comment Model
class Comment(models.Model):
    
    comment_text = models.CharField(max_length=200)
    
    author = models.ForeignKey(Profile, on_delete=models.CASCADE)
    
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    
    comment_date = models.DateField(auto_now=True)
    
    def __str__(self):
        return self.author
    
    
#checkout model
class Checkout(models.Model):
    
    check_user = models.CharField(max_length=30)
    
    item_id = models.IntegerField()
    
    def __str__(self):
        return self.name
    
    def getItem(self):
        item = Item.objects.get(id=self.item_id)
        return item
    
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['check_user', 'item_id'], name='unique item_id and user combo')
            ]


