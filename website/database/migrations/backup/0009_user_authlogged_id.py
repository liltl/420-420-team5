# Generated by Django 3.2 on 2021-05-21 12:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('database', '0008_auto_20210521_0736'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='authlogged_id',
            field=models.CharField(default=6, max_length=3),
            preserve_default=False,
        ),
    ]
