from django.contrib import admin

from .models import Profile, Item, Comment

# Register your models here.
admin.site.register(Profile)
admin.site.register(Item)
admin.site.register(Comment)