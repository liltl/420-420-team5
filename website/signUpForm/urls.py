# -*- coding: utf-8 -*-

from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from itemApp import views as item_views


urlpatterns = [
        path('', views.index, name="index"),
        path('login/', auth_views.LoginView.as_view(template_name='signUpForm/login.html'), name="login"),
        path('logout/', auth_views.LogoutView.as_view(template_name='signUpForm/logout.html'), name="logout"),
        path('profile/<int:pk>/', views.profile, name="profile"),
        path('register/', views.register, name="register"),
        path('registerSuccessful/', views.registerSuccess, name="registerSuccess"),
        path('index/', views.indexUser, name="indexUser"),
        path('profile/changePassword/', auth_views.PasswordChangeView.as_view(template_name='signUpForm/changePassword.html', success_url = '/'), name="changePassword"),
        path('profile/changeAvatar/', views.avatarChange, name="changeAvatar"),
        path('delete/', item_views.deleteItem, name="delete"),
        path('modify/', item_views.modifyItem, name="modify"),
        path('modifyConfirm/', item_views.modifyConfirm, name="modifyConfirm"),
        path('add/', item_views.addItem, name="add"),
        path('buyNow/', views.buyNow, name="buyNow"),
        path('checkout/', views.checkout, name="checkout"),
        path('commentView/<int:pk>/', views.commentView, name="commentView"),
        path('commentView/<int:pk>', views.addComment, name="addComment"),
        path('deleteCheckout', views.deleteCheckout, name="deleteCheckout"),
        path('search/', item_views.search, name="search"),
        
    ]
