# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from database.models import Item, Profile
from django.contrib.auth.forms import PasswordChangeForm


class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=30, help_text='Required field. 30 characters or fewer.')
    email = forms.EmailField(max_length=200, help_text='Required field.')
    first_name = forms.CharField(max_length=200)
    last_name = forms.CharField(max_length=200)
    
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')
        
        
        
class addItemForm(forms.ModelForm):
    
    class Meta:
        model = Item
        fields = ['owner', 'name', 'images', 'price', 'description']
        widgets = {
            'owner': forms.HiddenInput(),
        }
        

   
class addCommentForm(forms.Form):
    comment = forms.CharField(widget=forms.Textarea)


class changeAvatarForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('avatar',)
