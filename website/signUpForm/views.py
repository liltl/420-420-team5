from django.shortcuts import render, redirect

from .forms import SignUpForm, addItemForm, addCommentForm, changeAvatarForm

from django.contrib import messages
from django.contrib.auth import login, authenticate, update_session_auth_hash
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.contrib.auth.forms import PasswordChangeForm
from database.models import Profile, Item, Comment, Checkout
from django.utils.decorators import method_decorator
from django.views.generic.edit import UpdateView
from django.http import HttpResponseRedirect
from django.urls import reverse


# Create your views here.
def index(request):
    items = Item.objects.all()
    content = {'items': items}
    return render(request, 'signUpForm/index.html', content)

def log(request):
    return render(request, 'signUpForm/login.html')

def logout(request):
    return render(request, 'signUpForm/logout.html')

def profile(request, pk):
    
    user = Profile.objects.get(authlogged_id = request.user.id)
    
    items = Item.objects.filter(owner_id = user.id)
    
    checkout_items = Checkout.objects.filter(check_user = request.user.username)
    
    if request.method == 'POST':
        form = addItemForm(request.POST, request.FILES)
  
        if form.is_valid():
            form.save()
    else:
        form = addItemForm()
        
    content = {
        
        "currentUser": user,
        "items": items,
        "form": form,
        "checkout_items": checkout_items,
        
        }
    
    return render(request, 'signUpForm/profile.html', content)

"""
Method which validates the registration form that the user filled in.

"""
def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            email = form.cleaned_data.get('email')
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            
            addToDB(request, form)
            
            return redirect('registerSuccess')
    else:
        form = SignUpForm()
    # content = {}
    return render(request, 'signUpForm/register.html', {'form': form})

def registerSuccess(request):
    return render(request, 'signUpForm/registerSuccess.html')

def indexUser(request):
    
    currentUser = Profile.objects.get( authlogged_id = request.user.id)
    
    items = Item.objects.all()
    content = {'items': items, "user": currentUser,}
    return render(request, 'signUpForm/indexUser.html', content)

def passwordChange(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Password successfully updated.')
            return redirect('profile')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'signUpForm/changePassword.html', {'form': form})

def avatarChange(request):
    currentUser = Profile.objects.get( authlogged_id = request.user.id)
    form = changeAvatarForm(instance=currentUser)

    if request.method == 'POST':
        form = changeAvatarForm(request.POST, request.FILES, instance=currentUser)
        if form.is_valid():
            form.save()
    content = {"user": currentUser, "form": form}
    return render(request, 'signUpForm/changeAvatar.html', content)

def addToDB(request, form):
    newUser = Profile()
    newUser.first_name = form.cleaned_data.get('first_name')
    newUser.last_name = form.cleaned_data.get('last_name')
    newUser.authlogged_id = request.user.id
    newUser.username = form.cleaned_data.get('username')
    newUser.password = form.cleaned_data.get('password1')
    newUser.email = form.cleaned_data.get('email')
    newUser.status = "customer"
    
    newUser.save()


def buyNow(request):
    itemId = request.GET.get('Submit')
    if request.user.is_authenticated:
        Checkout(check_user=request.user.username, item_id=itemId).save()
        return render(request, 'signUpForm/checkout.html', {})
    else:
        return render(request, 'signUpForm/message.html', {})
    
def checkout(request):
    if request.user.is_authenticated:
        checkout_items = Checkout.objects.filter(check_user=request.user.username)
        new_owner = Profile.objects.get(username=request.user.username)
        for checkout_item in checkout_items:
            old_owner = checkout_item.check_user
            itemId = checkout_item.item_id
            item = Item.objects.get(id=itemId)
            item.owner = new_owner
            item.save()
            Checkout.objects.filter(check_user=old_owner, item_id=itemId).delete()
    return HttpResponseRedirect(reverse('profile', kwargs={'pk': request.user.id}))

def commentView(request, pk):
    
    currentItem = Item.objects.get(id=pk)
    
    try:
        comments = Comment.objects.filter(item_id=currentItem.id)
    except Comment.DoesNotExist:
        comments = None
     
    if request.method == "POST":
        form = addCommentForm(request.POST)
        
    else:
        form = addCommentForm()
    
    
    content = {
        "item": currentItem,
        
        "comments": comments,
        
        "form": form,
        
        }
    
    return render(request, 'signUpForm/commentView.html', content)




def addComment(request, pk):
    
    currentItem = Item.objects.get(id=pk)    
    
    if request.method == "POST":
        
        form = addCommentForm(request.POST)
        
        if form.is_valid():
    
            newComment = Comment()
            
            newComment.comment_text = form.cleaned_data['comment']
            
            newComment.author = Profile.objects.get(authlogged_id = request.user.id)
            
            newComment.item = currentItem
            
            newComment.save()

            #reloading the page
    
    #try:
    comments = Comment.objects.filter(item_id=currentItem.id)
    #except Comment.DoesNotExist:
     #   comments = None
    
    content = {
        "item": currentItem,
        
        "comments": comments,
        
        "form": form,
        }
    
    return render(request, 'signUpForm/commentView.html', content)

#Deleting selected item in checkout
def deleteCheckout(request):
    if request.user.is_authenticated:
        checkout_id = request.GET.get('checkoutId')
        Checkout.objects.filter(id=checkout_id).delete()
    return HttpResponseRedirect(reverse('profile', kwargs={'pk': request.user.id}))
