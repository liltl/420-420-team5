from django.db import models

# Create your models here.

class User(models.Model):
    user_name = models.CharField(max_length=30, default='')
    email = models.EmailField(max_length=200, default='')
    password = models.CharField(max_length=100, default='')
    first_name = models.CharField(max_length=100, default='')
    last_name = models.CharField(max_length=100, default='')
    avatar = models.ImageField(null=True, blank=True, upload_to="images/avatar/")
    
    def __str__(self):
        return self.user_name
