# Generated by Django 3.2 on 2021-05-19 16:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('signUpForm', '0003_auto_20210514_1621'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='birth_date',
        ),
    ]
