# 420-420-team5
# Python Kijiji mimic project


### Team members:
	- Sammy Pantaleon-Hernandez
	- Nicolae Rusu
	- David Tran
	- Liliane Tran Le
	
### About:
Kjan is a kijiji/ebay clone. It is an online marketplace that lets users register, post items alongside their price, and buy other users' items. Furthermore, users can comment on items as long as they are logged in. When buying, items are first transfered to your cart, and only after checking out do items transfer owner.


[Heroku website](https://djangoapp-team5website.herokuapp.com/)


[Admin page](https://djangoapp-team5website.herokuapp.com/admin)
	Username: admin
	Password: admin



The usernames for all users: (Password is kjantesting)

	1. test123

	2. test456

	3. customer1

	4. customer2
	
	5. customer3

